import '@mantine/core/styles.css';
import { MantineProvider, ColorSchemeScript } from '@mantine/core';
import { MainLayout } from '@/components/layouts/MainLayout';
import Providers from '@/components/providers';

export const metadata = {
  title: 'My test app',
  description: 'I have followed setup instructions carefully',
};

export default function RootLayout({ children }: { children: React.ReactNode }) {
  return (
    <html lang="ru">
      <head>
        <ColorSchemeScript />
      </head>
      <body>
        <Providers>
          <MantineProvider>
            <MainLayout>{children}</MainLayout>
          </MantineProvider>
        </Providers>
      </body>
    </html>
  );
}
