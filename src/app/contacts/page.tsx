import { ContactsData } from '@/components/contacts_data';

const Contacts: React.FC = () => {
  return (
    <>
      <ContactsData />
    </>
  );
};

export default Contacts;
