import ILink from '@/types';
export const links: ILink[] = [
  {
    id: 0,
    title: 'Main',
    url: '/',
  },
  {
    id: 1,
    title: 'Contacts',
    url: '/contacts',
  },
];
