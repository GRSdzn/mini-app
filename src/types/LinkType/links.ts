export interface ILink {
  id: number;
  title: string;
  url: string;
}
