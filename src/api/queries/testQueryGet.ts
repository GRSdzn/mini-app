import { useQuery } from '@tanstack/react-query';
import TestService from '../testGet';

export function useTestQuery() {
  return useQuery({
    queryKey: ['test'],
    queryFn: () => TestService.GetTestQuery(),
    refetchOnWindowFocus: false,
  });
}
