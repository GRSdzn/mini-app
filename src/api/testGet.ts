import { api } from './index';

export default class TestService {
  static async GetTestQuery() {
    return api({
      method: 'GET',
      url: `character/`,
    });
  }
}
