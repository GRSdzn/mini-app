import { QueryClient } from '@tanstack/react-query';
import axios from 'axios';

export const API_URL = process.env.NEXT_PUBLIC_HOST_API;
console.log(API_URL);
axios.defaults.withCredentials = true;
export const api = axios.create({
  baseURL: API_URL,
  timeout: 25000,
  withCredentials: false,
});

const queryClient = new QueryClient();

export { queryClient };
