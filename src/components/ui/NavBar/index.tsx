import { NavLinks } from './navLinks';

const NavBar: React.FC = () => {
  return (
    <>
      <NavLinks />
    </>
  );
};

export default NavBar;
