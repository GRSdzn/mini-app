'use client';
import React, { useState } from 'react';
import Link from 'next/link';
import { links } from '@/mock_data/mock_links';
import { Flex, NavLink } from '@mantine/core';
export const NavLinks: React.FC = () => {
  const [active, setActive] = useState(0);
  return (
    <>
      <Flex>
        {links.map((el) => (
          <NavLink label={el.title} w={'50%'} key={el.id} component={Link} href={el.url} fw={'bold'} active={el.id === active} variant="light" color="green" onClick={() => setActive(el.id)} />
        ))}
      </Flex>
    </>
  );
};
