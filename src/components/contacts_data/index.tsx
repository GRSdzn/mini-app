'use client';
import { useTestQuery } from '@/api/queries/testQueryGet';
import { IItems } from '@/types/Items/items';
import { NavLink, Paper } from '@mantine/core';
import Link from 'next/link';

export const ContactsData: React.FC = () => {
  const getData = useTestQuery();
  const { data, isLoading } = getData;
  // console.log(data?.data);
  return (
    <div>
      {isLoading ? (
        <div>Loading...</div>
      ) : (
        data?.data?.results.map((el: IItems) => (
          <Paper shadow="md" withBorder p="xl" key={el.id}>
            <NavLink label={el.name} component={Link} href={`/contacts/${el.id}`} fw={'bold'} description={el.status} variant="filled" color="rgba(83, 148, 134, 0.59)" />
          </Paper>
        ))
      )}
    </div>
  );
};
