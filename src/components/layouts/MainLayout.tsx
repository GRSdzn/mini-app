import React from 'react';
import NavBar from '../ui/NavBar';
export const MainLayout = ({ children }: { children: React.ReactNode }) => {
  return (
    <div>
      <NavBar />
      {children}
    </div>
  );
};
